import React, { Component } from 'react';
import Header from './header';
import Carousel from './carousel';
import Smartphone from './smartphone';
import Laptop from './laptop';
import Promotion from './promotion';

class Home extends Component {
    render() {
        return (
            <div>
                <Header/>
                <Carousel/>
                <Smartphone/>
                <Laptop/>
                <Promotion/>
            </div>
        );
    }
}

export default Home;